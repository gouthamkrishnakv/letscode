# Let's Code 2021 
### Conducted by SAC, CSEA, FOSSCell - NIT Calicut

Let's Code is an initiative by the Students Affair Council (SAC) of NIT Calicut which is conducted in association with Computer Science and Engineering Association (CSEA) and FOSSCell to help students from non-CS branches learn problem solving and coding. 

## Pre-requisites

- hugo : Refer this for installation setup - [Hugo Setup Guide](https://gohugo.io/getting-started/installing/)


## Development Setup

```
git clone git@gitlab.com:afeedh/letscode.git
```

```
cd letscode
```

```
git submodule update --init --recursive 
```

```
hugo server -D
```

The Hugo Web Server will be available at [http://127.0.0.1:1313/](http://127.0.0.1:1313/)

## Contributing Session Docs

To add a session doc, go to `/content/post/` and add the doc as a markdown file (for eg: session_4.md) with the contents similar to this:

```
+++
author = "Person 4"
title = "Session 4"
date = 2021-04-18
description = "A brief description of Hugo Shortcodes"
tags = [
    "shortcodes",
    "privacy",
]
+++

Session 3 description
<!--more-->
Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
```

